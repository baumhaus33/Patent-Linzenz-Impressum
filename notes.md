# Lizenz
* Wozu sind Lizenzen
* Welche Arten von Lizenzen gibt es ?
* Was kann lizenziert werden ?
* Was sind Opensource Lizenzen und welche Arten gibt es ?

# Patent
* Was ist ein Patent ?
* Wie kann ich ein Patent erstellen ?
* Wo gilt mein Patent ?
* Was kostet es ein Patent zu nutzen
* Was kann nicht patentiert werden ?
